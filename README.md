**SkyWalking使用教程**
-   
说明：skywalking服务器端可以通过mysql、ElasticSearch、和 h2存储数据
     官方推荐使用ElasticSearch(尽量使用 es6.0)
    
- 首先安装ElasticSearch,这里使用docker-compose安装

  1、新建一个docker-compose.yml文件，文件内容如下：
     ```
        version: '3.3'
        services:
          elasticsearch:
            image: wutang/elasticsearch-shanghai-zone:6.3.2
            container_name: elasticsearch
            restart: always
            ports:
              - 9200:9200
              - 9300:9300
            environment:
              cluster.name: elasticsearch
     ```
  2、执行 docker-compose up -d 启动es
  
  3、测试es是否启动成功
  
      在浏览器窗口访问 http://127.0.0.1:9200 能看到es返回信息即表示es启动成功。
      
  4、下载 SkyWalking
   
      下载地址  http://skywalking.apache.org/downloads/
      
      该demo使用的是apache-skywalking-apm-incubating-6.0.0-beta版本
      
  5、修改SkyWalking配置
      
      下载完成后解压SkyWalking
      进入 apache-skywalking-apm-incubating/config 目录并修改 application.yml 配置文件
      
    ![skywalking配置图](./images/skywalking.png)
     
      这里需要做的事情
        - 注释 H2 存储方案
        - 启用 ElasticSearch 存储方案
        - 修改 ElasticSearch 服务器地址
  
   6、启动 SkyWalking
   
    - 修改完配置后，进入 apache-skywalking-apm-incubating\bin 目录，运行 startup.bat 启动服务端
 
     ![skywalking配置图](./images/skywalking01.png)
     
    -通过浏览器访问 http://localhost:8080 出现如下界面即表示启动成功
       
     ![skywalking配置图](./images/skywalking02.png)  
    
    -默认的用户名密码为：admin/admin，登录成功后，效果如下图
    
     ![skywalking配置图](./images/skywalking03.png) 
     
   7、以上步骤完成则SkyWalking服务器端配置完成
   
   
   
 - 配置SkyWalking客户端
   1、Java Agent 服务器探针 
   
      我们需要使用官方提供的探针为我们达到监控的目的，按照实际情况我们需要实现三种部署方式
      
      - IDEA 部署探针
      - Java 启动方式部署探针（我们是 Spring Boot 应用程序，需要使用 java -jar 的方式启动应用）
      - Docker 启动方式部署探针
      
      探针文件在 apache-skywalking-apm-incubating/agent 目录下
      
      ![skywalking配置图](./images/agent.png) 
      
      **IDEA 部署探针**
      
      在工程路径下创建一个名为skywalking-agent 的目录，并将 agent 整个目录拷贝进来
 
      ![skywalking配置图](./images/agent01.png) 
      
      修改项目的 VM 运行参数，点击菜单栏中的 Run -> EditConfigurations...，此处我们以 skywalking-provider 项目为例，修改参数如下
      
      ```
        -javaagent:D:\yyl\code\skywalking-demo\skywalking-agent\agent\skywalking-agent.jar
        -Dskywalking.agent.service_name=app-provider
        -Dskywalking.collector.backend_service=localhost:11800
   
        javaagent:表示探针的绝对路径
        Dskywalking.agent.service_name 表示服务名称
        Dskywalking.collector.backend_service 表示连接SkyWalking服务器端地址
        
      ```
   
      ![skywalking配置图](./images/agent02.png) 
      
      - -javaagent：用于指定探针路径
      - -Dskywalking.agent.service_name：用于重写 agent/config/agent.config 配置文件中的服务名
      - -Dskywalking.collector.backend_service：用于重写 agent/config/agent.config 配置文件中的服务地址
    
    **Java 启动方式**
    ```
        java -javaagent:D:\yyl\code\skywalking-demo\skywalking-agent\agent\skywalking-agent.jar -Dskywalking.agent.service_name=app-provider -Dskywalking.collector.backend_service=localhost:11800 -jar yourApp.jar
    ```
   
   **测试监控**
   启动 skywalking-provider 项目;
   访问之前写好的接口 http://localhost:8082/provider 之后再刷新 SkyWalking Web UI，你会发现 Service 与 Endpoint 已经成功检测到了
   
   ![skywalking配置图](./images/agent03.png) 
   
   ![skywalking配置图](./images/agent04.png) 
   
   **至此，表示 SkyWalking 链路追踪配置成功**
   
  ***********************************
   
   
   
   **如果skywalking服务器端要使用mysql存储，需要进行以下操作**
   
   ![skywalking配置图](./images/skywalking.png)
   注释掉h2和es代码 ，放开mysql注释； 
   并且修改apache-skywalking-apm-incubating/config 目录下的 datasource-settings.properties 文件
   
   ```
        只需要修改mysql连接地址、用户名和密码即可

        jdbcUrl=jdbc:mysql://106.12.15.149:3306/swtest
        dataSource.user=root
        dataSource.password=123456

   ```
   ![skywalking配置图](./images/skywalking06.png)
   
   然后通过通过数据库连接工具 创建swtest库即可；
   数据库中的表在启动时会自动初始化
   
   修改完配置后，进入 apache-skywalking-apm-incubating\bin 目录，运行 startup.bat 启动服务端
   
   
   
   
 
 
   
   
      
 