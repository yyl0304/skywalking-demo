package com.daxuexiu.controller;

import com.daxuexiu.service.ConsumerFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: yueylong
 * @Create: 2019-12-19 21:20
 * @Desc:
 */
@RestController
public class ConsumerController {

    @Autowired
    private ConsumerFeign consumerFeign;

    @Value("${server.port}")
    private String port;

    @GetMapping("/demo")
    public String demo(){

        String message = consumerFeign.provider();


        return "调用消费者模块,"+ message + ";调用生产者结束";
    }
}
