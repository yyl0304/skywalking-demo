package com.daxuexiu.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @Author: yueylong
 * @Create: 2019-12-19 23:37
 * @Desc:
 */
@FeignClient(value = "app-provider")
public interface ConsumerFeign {

    @GetMapping("/provider")
    public String provider();
}
