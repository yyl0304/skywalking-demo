package com.daxuexiu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @Author: yueylong
 * @Create: 2019-12-19 21:10
 * @Desc:
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class AppConsumer {

    public static void main(String[] args) {
        SpringApplication.run(AppConsumer.class,args);
    }
}
