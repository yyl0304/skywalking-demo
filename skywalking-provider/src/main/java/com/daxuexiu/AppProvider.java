package com.daxuexiu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @Author: yueylong
 * @Create: 2019-12-19 21:20
 * @Desc:
 */
@SpringBootApplication
@EnableDiscoveryClient
public class AppProvider {

    public static void main(String[] args) {
        SpringApplication.run(AppProvider.class,args);
    }
}
