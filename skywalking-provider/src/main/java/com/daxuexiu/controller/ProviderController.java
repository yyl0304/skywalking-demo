package com.daxuexiu.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: yueylong
 * @Create: 2019-12-19 21:34
 * @Desc:
 */
@RestController
public class ProviderController {

    @Value("${server.port}")
    private String port;

    @GetMapping("/provider")
    public String provider(){
        return "我是生产者;端口为:" + port;
    }
}
