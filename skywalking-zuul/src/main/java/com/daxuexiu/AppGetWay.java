package com.daxuexiu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * @Author: yueylong
 * @Create: 2019-12-19 21:52
 * @Desc:
 */
@SpringBootApplication
@EnableZuulProxy
@EnableDiscoveryClient
public class AppGetWay {

    public static void main(String[] args) {
        SpringApplication.run(AppGetWay.class,args);
    }
}
